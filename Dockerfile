FROM python:3.11 as test

COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt
COPY src /src
WORKDIR /src

RUN python test.py

FROM python:3.11-slim as deploy

COPY src /src
WORKDIR /src

ENTRYPOINT [ "python" ]
CMD [ "main.py" ]